<?php

namespace App\Http\Controllers;

use App\Feature;
use App\Products_images;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use File;
use Image;


class PublishController extends Controller
{
    public function publish(Request $request){

       $publication  =  new Feature();

        $data = json_decode($request->input('data'));
        $collection = collect(json_decode($request->input('data')));

        $publication->code_id = $data->basic->code_id;
        $publication->detail  = $data->basic->detail;

     
        if(collect($collection['basic'])->has('video')){

            $publication->video = $data->basic->video;
        }
   


        if(!empty($collection['features'])){

            $publication->features= json_encode($data->features);

        }


       $publication->save();

        //inicio imagenes
        $image      =   $request->file('image');
        //$user= new imagenmodelo($request->all());
        if ($image){

            $carpeta    =   time();

            File::makeDirectory("upload/"."carpeta".$carpeta , 0777, true);

            foreach ($image as $ima ) {
                $name = $ima->getClientOriginalName();//nombre original 

                
                $path_base  =   'upload/'.'carpeta'.$carpeta;

             //    /carpeta'.time() anexo
                
                \Storage::disk('upload')->put("carpeta".$carpeta."/large/".$name,  \File::get($ima)); //guardar imagenes original Large

                $pub_images= new Products_images($request->all());
                $pub_images->path           =   $path_base;
                $pub_images->name           =   $name;
                $pub_images->product_code = $data->basic->code_id;
                $pub_images->save();

            }


            //guardando imagen small

            File::makeDirectory("upload/"."carpeta".$carpeta."/small" , 0777, true);//creacion de carpeta para imagen tipo small
            
            foreach ($image as $ima){
             
                getimagesize($ima) ;
                image_proportion(getimagesize($ima),100,0);
                $newSize = image_proportion(getimagesize($ima),300,0);

                $new_file   =   Image::make($ima)->resize($newSize['ancho'],$newSize['alto']);

                
                $name       =    $ima->getClientOriginalName();

                $path       =   'upload/'.'carpeta'.$carpeta.'/small/'.$name;  //ruta
             

                $new_file->save($path); //se guarda en la ruta

            }

            // guardando imagen thumb

            File::makeDirectory("upload/"."carpeta".$carpeta."/thumb" , 0777, true);  //creacion de carpeta para imagen tipo thumb

            foreach ($image as $ima){
             
                getimagesize($ima) ;
                image_proportion(getimagesize($ima),100,0);
                $newSize = image_proportion(getimagesize($ima),75,0);

                $new_file   =   Image::make($ima)->resize($newSize['ancho'],$newSize['alto']);

                $name       =    $ima->getClientOriginalName();

                $path       =   'upload/'.'carpeta'.$carpeta.'/thumb/'.$name;  //ruta

                $new_file->save($path);

            }
        
              
        }
        
        return redirect()->route('detalleproducto', $publication->code_id  );


    }

    public function destroy($id)
    {
    	$feature = Feature::where('code_id',$id);	
        $feature->delete(); 
        return redirect()->route('detalleproducto', $id);
    }



}
