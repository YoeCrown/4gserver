var cart = angular.module('cart',[]);


 cart.controller('cartController',['$scope','$http',function($scope,$http){

    $scope.baseUrl = 'http://192.168.1.11/laravel/public/';
    $scope.itemList = Array;
    $scope.updateStatus = false;



   $scope.addItem = function(code,$index){
        console.log("antes" + code);
        $http.get($scope.baseUrl + 'cart/add/'+code).then(function(data) {
        
            console.log("Data recibida");
            console.log(data.data);
            

                        
            console.log("Validando que el elemento no exista, si no existe se agrega");

            if (!$scope.exist(data.data.code)){
                console.log("No existe, agrgado uno al array");
                $scope.itemList.push(data.data);
            }
            else
            {
                console.log("Si existe, incrementado en 1 el elemento");
                $scope.incrementExist(data.data.code)
            }

            console.log($scope.itemList);

            $scope.updateCart();
            $.notify("Se ha agregado un item al carrito",{ position:"bottom right" ,className: 'success', gap: 4 });

         }) 
        
    };


    $scope.exist = function($code){
        
        for(item in $scope.itemList){
            if($scope.itemList[item].code == $code)
            {
                return true
            }
        }

        return false;
    }

        $scope.incrementExist = function($code){
        
        for(index in $scope.itemList){
            if($scope.itemList[index].code == $code)
            {   
                // console.log('Elemento incrementado ++');
                // console.log($code);
                $scope.itemList[index].qty ++;
                break;
            }
        }

    }


    $scope.totalAmount = function(){
        var total = 0;
        for(item in $scope.itemList){

           total  += ($scope.itemList[item].price * $scope.itemList[item].qty);
          
        }

        return total;
    }

    $scope.updateQty = function(idx,qty){

        $scope.updateStatus = true;
        
        setTimeout(function(){
            $scope.updateCart()
            $scope.updateStatus = false;
        },2000);
        
    }

    $scope.updateCart = function (){
        
        $scope.updateStatus = true;

        var cartData = JSON.stringify($scope.itemList);

         $http.post($scope.baseUrl + 'cartUpdateJson', {'json' : cartData })
            .then(function (data, status, headers, config) {
                $scope.PostDataResponse = data;
                $scope.updateStatus = false;
            })
            
         
    }


    $scope.deleteItem = function(index){

        $scope.itemList.splice(index, 1)
        $scope.updateCart();
        $.notify("Se ha Eliminado un item al carrito",  { position:"bottom right" });
        
    }

    $scope.trash = function(){
         $scope.itemList = [];
         $scope.updateCart();
         $.notify("Se ha Vaciado  el carrito",  { position:"bottom right" , className: 'info', });
    }


    $http.get($scope.baseUrl + 'cartJson').then(function(data) {
          
        $scope.itemList = data.data;
        console.log("#####################");
            console.log("Data inicial")
            console.log(data.data);
        console.log("#####################");
    })

    // hambre

    $scope.featureList = [];
    $scope.titleFeature = '';

    $scope.pub = {};

    $scope.catPreview = {};

    
$scope.setUp = {
            'basic' : $scope.pub,
            'features' : $scope.featureList
        }

    $scope.tempFeatureListStatus = false;
    $scope.featuresDetected = false;
    $scope.detectFeatures = function()
    {

        $http.get($scope.baseUrl + 'api/detectFeatures/' + $scope.pub.title )
        .then(function(data, status, headers, config){
        
        if ($scope.featureList.length == 0){

          if (data.data.length)
          { 
            $scope.tempFeatureListStatus    = true;
            $scope.featuresDetected         = true;
            $scope.tempFeatureList          = data.data;
          }
          else
          { 
            console.log('caracteristicas no detectadas')
            $scope.tempFeatureList  = data.data;
            $scope.featuresDetected = true;
          }
        }
        else{

            $scope.featuresDetected = true;
        }

          

        })

    }

    $scope.applyFeatures = function(){

        $scope.featureList      = $scope.tempFeatureList;
        $scope.setUp.features   = $scope.featureList;
        $scope.tempFeatureListStatus = false;
    }

    $scope.disApplyFeatures = function(){

        $scope.featureList = [];
        $scope.tempFeatureListStatus = false;
    }


    $scope.publish = function(){

        console.log($scope.featureList);

        $scope.setUp.basic      = $scope.pub;
        $scope.setUp.features   = $scope.featureList;

        
        console.log($scope.setUp);
        formData.submit();

    }

    $scope.fackefilebutton = function(){
        images.click();
    }

        

        
          


    $scope.addFeature = function(){
        $scope.featureList.push({
            'label': $scope.titleFeature,
            'type' : '', // Aqui se especifica el tipo de caracteristica, fecha, numero, etc
            'values' : []
        });
        
        $scope.titleFeature = '';
    };

    
    
    $scope.addFeatureValues = function(index,value)
    {   
        /*
            Add value to feature list
        */
        
        if (value) {
            $scope.featureList[index].values.push({value});
        }
    }


    $scope.deleteFeature = function(index){
        $scope.featureList.splice(index, 1)
    }

    


 }]);