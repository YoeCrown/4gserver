<?php

namespace App\Http\Controllers;

use App\Products;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\File;

class ImportController extends Controller
{
     public function import(Request $request)
    {

    // Get csv file, save and read
    $file = $request->file('archivo');
    $nombre = time().'_'.$file->getClientOriginalName();
    \Storage::disk('csv')->put($nombre,  \File::get($file));
    Excel::load('csv/'.$nombre, function($reader) {
 

        foreach ($reader->get() as $product) {

            $result = Products::find($product->code);
            
               
           
            // echo "Evaluando codigo de csv producto es igual a $product->code";
            
            

            // echo "<br>########<br>";

            if(@$result->code == $product->code ){ 

                        
                        if ($result->url_image  !==   $product->url_image)
                        {

                            /*  If the image isent equal to the old item 
                                Download and put the url file
                            */

                            $content = file_get_contents($product->url_image);
                            $file_name = basename($product->url_image); 
                            \Storage::disk('upload')->put($file_name, $content);

                            $result->url_image    =  $product->url_image;
                            $result->name_image   =  $file_name;

                        }
                        

                         $result->item             =   $product->item;
                         $result->qty              =   $product->qty;
                         $result->description      =   $product->description;
                         $result->price            =   $product->price;
                         $result->tax              =   $product->tax;
                         $result->precio_compra    =   $product->precio_compra;
                         $result->ganancia         =   $product->ganancia;
                         $result->precio_colombia  =   $product->precio_colombia;
                         $result->proveedor        =   $product->proveedor;
                        
                         $result->update(); 
           
  

                }
             else{ 

                    // echo "Guardando $product->code <br> <br> ";
                    // $file_name = '';
                    // Download and put the url file
                    $content = file_get_contents($product->url_image);
                    $file_name = basename($product->url_image); 
                    \Storage::disk('upload')->put($file_name, $content);

                    @$result->url_image    =   $product->url_image;
                    @$result->name_image   =   $file_name;
                    // End download
                

                    # Create a new product

                    Products::create([
                    'code'          =>  $product->code,
                    'item'          =>  $product->item,
                    'description'   =>  $product->description,
                    'url_image'     =>  $product->url_image,
                    'name_image'    =>  $file_name,
                    'qty'           =>  $product->qty,
                    'price'         =>  $product->price,
                    'tax'           =>  $product->tax,
                    
                    'precio_compra'    =>   $product->precio_compra,
                    'ganancia'         =>   $product->ganancia,
                    'precio_colombia'  =>   $product->precio_colombia,
                    'proveedor'        =>   $product->proveedor


                 ]);

             }   

        }
    });
        # Redirect to succesfully upload
        return redirect('notify/csv-import-success');
     }
}
