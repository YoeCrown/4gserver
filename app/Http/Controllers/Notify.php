<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\Mail\Proforma;
use App\Mail\ProformaCompany;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Message;

class Notify extends Controller
{
    
	function register_success()
	{	

		return view('notify/register-success');
	}

	function csv_import_success()
	{	

		return view('notify/csv-import-successfuly');
	}

	function proforma_success()
	{	

		return view('notify/proforma-success');
	}


}
