@extends('layouts.app')

@section('content')

  
  <div class="container">
            <form 
            action="publish" 
            method="post" 
            enctype="multipart/form-data" 
            id='formData' 
            style="display:none"
            onsubmit="event.preventDefault()" 
            >
              {{csrf_field()}}
              <input type="text" name="data" value="@{{setUp}}">
           </form>
  
           <nav  ng-show='!searchResult'>
             <div class="nav-wrapper ">
              <h4 class="brand-logo createnav" >Completa la información del formulario para configurar tu publicacion. <a class="btn btn-success" ng-click="publish()">Publicar!</a>
 </h4>
                          
             </div>
           </nav>  
       
          <div class="row">
              
               <div class="input-field col-md-12">
                <label for="detail">Detallado</label>
                <textarea id="detail" class="form-control" ng-model="pub.detail"></textarea>

                <input type="hidden"  id="detail" ng-model="pub.code_id=2" >
               </div>

        
                <div class="col-md-6">
                    <div class="card">
                      <div class="card-content">
                        <span class="card-title">Propiedades de tu publicacion</span>
                          <div class="row">
                            <div class="row" >
                              <div class="col-md-2">
                              <br>
                                  <a class="btn btn-info" ng-click='addFeature()'>
                                    agregar
                                  </a>
                              </div><br>

                              <div class="col-md-10" >
                                <div>
                                  <input id="titleFeature" type="text" ng-model='titleFeature'>
                                  <label for="titleFeature" class="" > Ejemplo: Ancho,Alto,Color</label>
                                </div>        
                              </div>

                            </div>


                            <style type="text/css">
                              table{
                                padding: 0px;
                                border-collapse: collapse;
                                border:solid 1px #c5c5c5;
                              }
                              table td{border:solid 1px #c5c5c5;padding: 5px;border-radius: 0px}
                              .shadow{
                                -moz-box-shadow:inset 0 0 5px #000000;
                                -webkit-box-shadow:inset 0 0 5px #000000;
                                box-shadow:inset 0 0 5px #000000;
                              }
                              .input-feature:focus{border:none;}
                            </style>
                        
                            <div ng-repeat='feature in featureList'>
                                <table class="">
                                  <tbody >
                                  <tr>
                                    <td><b>@{{feature.label}}</b></td>
                                    <td ng-click="deleteFeature($index)" style="padding: 0px;text-align:center;background-color: #414141;color:white;cursor: pointer;">x</td>
                                  </tr>
                                  <tr>
                                    <td>
                                      <input  id="tempFeature@{{$index}}" 
                                              type="text"
                                              ng-model="item[$index]"
                                              ng-blur="addFeatureValues($index,item[$index]); item[$index] = ''"
                                      >
                                    </td>
                                    <td ng-click="addFeatureValues($index,item[$index]); item[$index] = ''" style="padding-left: 5px;text-align:center;background-color: #086e18;color:white;cursor: pointer;">+</td>
                                  </tr>
                                    <tr>
                                      <td style="font-size: 11px;">
                                        <div class="" ng-repeat='child in feature.values'>
                                          @{{child.value}}
                                          <span ng-click="feature.values.splice($index, 1)" style="font-size: 11px">x</span>
                                          
                                        </div>
                                      </td> 
                                      
                                    </tr>
                                  </tbody>
                                  <tr>
                                    
                                  </tr>
                                </table>
                                <br>
                            </div>  
                          </div>

                      </div>
                    </div>
                </div>
          </div>

  </div>


@endsection