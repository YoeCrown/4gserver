<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body 
 style="padding: 10px;">

<div style="
	width: 100%;
	height: 130px;
	background-image: url('http://4gserver.com/images/logoa.jpg');
	background-repeat: no-repeat;
	">
</div>
	
	<div style="padding:10px">

	<p>
		Hola Sr/Sra <b>{{ $data['full_name']}}</b> hemos creado una preforma para usted, para comenzar con la compra de pedido.
	<br>
		Codigo de orden : <b>4mwc4273</b> <br>
		Metodo de pago : <b>{{$data['method_pay']}}</b> <br>
		Monto a pagar : <b>${{$data['total_amount']}}</b>

	</p>

	<p>Solo debes informar el pago indicando el codigo de tu transaccion, haciendo click en el siguiente enlace.</p>
	


	<h1 style="font-size:22px;"><b>Detalle de tu orden</b></h1>


	
	<?php

		$table_style 	= "width: 100%;text-align:right;";
		$th_style 		= "background:blue;color:white;border-radius:0px;padding:3px;text-align:right;";
		$td_style 		= "background:#f2f2f2;color:#000612;border-radius:0px;padding:3px;text-align:right;";

	?>

	<table style="<?php echo $table_style ?>">
		<thead>
			<tr>
			<th style="<?php echo $th_style ?>" >
				Producto
			</th>
			<th style="<?php echo $th_style ?>">
				Cantidad
			</th>
			<th style="<?php echo $th_style ?>">
				IVA
			</th>
			<th style="<?php echo $th_style ?>">
				Precio
			</th>

		</tr></thead>
	<tbody>
		<?php 
			foreach ($data['products'] as  $value) {	 ?>
		<tr>
			    
					
			<td style="<?php echo $td_style ?>">{{$value->item }}</td>
			<td style="<?php echo $td_style ?>">{{$value->qty }}</td>
			<td style="<?php echo $td_style ?>">{{$value->tax }}</td>
			<td style="<?php echo $td_style ?>">${{$value->price }}</td>

			
		</tr>
		<?php }?>
	</tbody>
	</table>
	

	<hr style="border:solid 1px beige">
	

	<center>
	<p style="font-size:10px;color:grey">
	<b>Nota:</b> Nosotros no le solicitaremos en ningun momento ni bajo ningun concepto los datos de sus tarjetas de credito ni ningun tipo de informacion personal, las transacciones que usted realice debe aplicarlas desde su pasarela de pago preferida.
	</p>
		
	
    <?php
    	switch ($data['method_pay']) {
    		case 'Paypal':
    			 echo "<h2><b>El Metodo de Pago seleccionado por usted fue Paypal puede hacer su pago a este correo electronico juliampt@hotmail.es  y luego contactarnos para verificar su transaccion. <b></h2> <br>";
    			break;
    		case 'Tranference':
    			 echo "<h2><b>El Metodo de Pago seleccionado por usted fue Transference  Por  Banco Pinchicha puede hacer su pago a esta cuenta y luego contactarnos para verificar su transaccion. <b></h2> <br>
    			 		<p><b><i>PICHINCHA AHORROS</i></b></p>
    			 		<p><b><i>Numero cuenta: 2202854410 </i></b></p>
    			 		<p><b><i>Nombres: JULIAN ANDRES </i></b></p>
    			 		<p><b><i>Apellidos: PERDOMO TRUJILLO </i></b></p><br>
    			 ";
    			break;
    		case 'Skrill':
    			 echo "<h2><b>El Metodo de Pago seleccionado por usted fue Skrill puede hacer su pago a este correo electronico juliampt@hotmail.es  y luego contactarnos para verificar su transaccion. <b></h2> <br>";
    			break;
    		case 'Bancolombia':
    			 echo "<h2><b>El Metodo de Pago seleccionado por usted fue Bancolombia puede hacer su pago a esta cuenta y luego contactarnos para verificar su transaccion. <b></h2> <br>
    			 		<p><b><i> BANCOLOMBIA AHORROS</i></b></p>
    			 		<p><b><i>Numero cuenta: 17419640891</i></b></p>
    			 		<p><b><i>Nombres: JULIAN ANDRES </i></b></p>
    			 		<p><b><i>Apellidos: PERDOMO TRUJILLO </i></b></p><br>
    			 ";
    			break;
    		case 'Bitcoin':
    			 echo "<h2><b>El Metodo de Pago seleccionado por usted fue Bitcoin puede hacer su pago a esta direccion de cartera <span style='color:yellow'> 16y3tMTKrTs7d2zKG2fsfthsUPPdbt7etp </span> y luego contactarnos para verificar su transaccion. <b></h2> <br>";
    			break;
    		
    		default:
    		 	echo "";

    			break;
    	}


    ?>


	<a 	href="#" 
		style="
			background:blue;
			color:white;
			border-radius:15px;
			padding:10px;
			font-size:22px;
			display:block;
			width:200px;
			text-align:center
			">
	INFORMAR PAGO!

		<br>

		<img style="width:100px" src="http://hambregram.com/landingpage/static/images/email/tarjetas.png">

	</a>
	
	<br>


		<hr style="border:solid 1px beige">

		<p style="font-size:16px">Puedes ponerte en contacto con nosotros por los siguientes medios: <br>
		<span style="font-size:11px">
			<b><a href="mailto:admin@4gserver.com" target="_blank">admin@4gserver.com</a></b>
			<br>
			<b><a href="#" value="#" target="#">+593 984217755  , 0984217755</a></b>
			<br>
			<b>Skype: juliampt</b>
		</span>
		</p>
	</center><div class="yj6qo"></div><div class="adL">
	

	

</div>






</body>
</html>