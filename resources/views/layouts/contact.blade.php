<div class="footer-v1">
            <div class="footer">

                <div class="container">
                    <div class="row">
                        <!-- About -->
                        <div class="col-md-4 md-margin-bottom-40">
                            <a class="footer-logo" href="index.html"> 4GSERVER.COM</a>
                            <p>4GSERVER.COM Es El Sitio donde tu podras comprar todo lo relacionado con Reparacion de Celulares, Herramientas de Desbloqueo, Partes y Accesorios. Nuestro lema   es y sera siempre  MEJORAMOS CUALQUIER PRECIO.</p>
                        </div><!--/col-md-3-->
                        <!-- End About -->



                        <!-- Link List -->
                        <div class="col-md-3 md-margin-bottom-40">
                            <div class="headline"><h2>Links Rapidos</h2></div>
                            <ul class="list-unstyled link-list">
                                <li><a href="#">Iniciar sesión</a><i class="glyphicon glyphicon-chevron-right"></i></li>
                                <li><a href="#">Registrarte</a><i class="glyphicon glyphicon-chevron-right"></i></li>
                                <li> <a href="{{route('products')}}">Productos</a><i class="glyphicon glyphicon-chevron-right"></i></li>
                                <li><a href="#">Contactenos</a><i class="glyphicon glyphicon-chevron-right"></i></li>
                            </ul>
                        </div><!--/col-md-3-->
                        <!-- End Link List -->

                        <!-- Address -->

                        <div class="col-md-5 map-img md-margin-bottom-40">
                            <div class="headline"><h2>Contactenos</h2></div>
                            <address class="md-margin-bottom-40">
                                <strong>Correo: </strong> <a href="#" class="">juliampt@hotmail.es</a><br>
                                <strong>Skype: </strong> juliampt<br>
                                <strong>Wechat: </strong> juliampt1989<br>
                                <strong>Whatsapp :</strong> +593984217755 , 0984217755  <br> 
                                <a target="_blank" href="https://api.whatsapp.com/send?phone=593984217755&amp;text=Saludos%204gserver%20quiero%20solicitar%20informacion%20"><strong style="color:yellow">Hablanos al Whatsapp  haz Click Aqui </strong> <i class="fa fa-whatsapp fa-2x" aria-hidden="true" style="color:#18ba9b"></i></a>
                            </address>
                        </div><!--/col-md-3-->
                        <!-- End Address -->

                    </div>
                </div>
            </div><!--/footer-->
            <div class="copyright2">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 linea">
                            <div class="col-md-12">
                                <p class="color">
                                    ACEPTAMOS TODOS LOS MEDIOS DE PAGOS 
                                </p>
                            </div>
                            <div class="col-xs-2">
                                <img class="img-responsive ima" src="{{url('images/Bitcoin.png')}}" style="margin-top: 21px; border:">
                            </div>
                            <div class="col-xs-2">
                               <img class="img-responsive ima" src="{{url('images/pichincha-logo.png')}}" style="margin-top: 10px; ">
                            </div>
                            <div class="col-xs-2">
                                <img class="img-responsive ima" src="{{url('images/PayPal.svg.png')}}" style="margin-top: 15px; ">
                            </div>
                            <div class="col-xs-2">
                                <img class="img-responsive ima" src="{{url('images/Skrill_primary_logo_RGB.svg.png')}}" style=" ">
                            </div>
                            <div class="col-xs-2">
                                <img class="img-responsive ima" src="{{url('images/2000px-Logo_Bancolombia.svg.png')}}" style="margin-top: 10px;">
                            </div>
                            <div class="col-xs-2">
                                <img class="img-responsive ima" src="{{url('images/efecty_transo.png')}}" style="margin-top: 10px;">
                                
                            </div>                         
                        </div>

                    </div>
                </div>
            </div><!--/copyright-->
            <div class="copyright">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <p>
                                2017 © Todos Los Derechos Reservados.
                            </p>
                        </div>

                    </div>
                </div>
            </div><!--/copyright-->      
    </div>