<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
  	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.0/css/materialize.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body 
 style="padding: 10px;">

<div style="
	width: 100%;
	height: 130px;
	background-image: url('http://4gserver.com/images/logoa.jpg');
	background-repeat: no-repeat;
	">
</div>
	
	<div style="padding:10px">

	<p>
		Se ha creado una proforma de solicitud con los siguientes datos.
	<br>
		Codigo de orden : <b>4mwc4273</b> <br>
		Nombre : <b>{{$data['full_name']}}</b> <br>
		Metodo de pago : <b>{{$data['method_pay']}}</b> <br>
		Monto a pagar : <b>${{$data['total_amount']}}</b>

	</p>

	


	<h1 style="font-size:22px;"><b>Detalle de tu orden</b></h1>


	
	<?php

		$table_style 	= "width: 100%;text-align:right;";
		$th_style 		= "background:blue;color:white;border-radius:0px;padding:3px;text-align:right;";
		$td_style 		= "background:#f2f2f2;color:#000612;border-radius:0px;padding:3px;text-align:right;";

	?>

	<table style="<?php echo $table_style ?>">
		<thead>
			<tr>
			<th style="<?php echo $th_style ?>" >
				Producto
			</th>
			<th style="<?php echo $th_style ?>">
				Cantidad
			</th>
			<th style="<?php echo $th_style ?>">
				IVA
			</th>
			<th style="<?php echo $th_style ?>">
				Precio
			</th>

		</tr></thead>
	<tbody>
		<?php 
			foreach ($data['products'] as  $value) {	 ?>
		<tr>
			    
					
			<td style="<?php echo $td_style ?>">{{$value->item }}</td>
			<td style="<?php echo $td_style ?>">{{$value->qty }}</td>
			<td style="<?php echo $td_style ?>">{{$value->tax }}</td>
			<td style="<?php echo $td_style ?>">${{$value->price }}</td>

			
		</tr>
		<?php }?>
	</tbody>
	</table>
	

	<hr style="border:solid 1px beige">
	



</div>






</body>
</html>