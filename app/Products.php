<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = ['code', 'item','description', 'detail','url_image','name_image','qty','price','tax'];

}
