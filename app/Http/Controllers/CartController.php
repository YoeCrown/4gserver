<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Products;
use App\Mail\Proforma;
use App\Mail\ProformaCompany;
use Illuminate\Support\Facades\Mail;
use Illuminate\Mail\Message;

class CartController extends Controller
{
    public function __construct(){

        if(!\Session::has('cart')) \Session::put('cart',array());
    }    
    
      //show cart
    public function show(){

        return view('cart');

    }



    //udatejsoncart

    public function cartUpdateJson(Request $request){
        
        $input = $request->all();
        \Session::put('cart' , $input['json']);

        return $input['json'];
        

    }

     public function cartJson(){

        // \Session::forget('cart');

        $cart =  \Session::get('cart');

        if (!$cart) {
            
            return '[]';
        }
        else
        {
            return $cart;
        }
        

        

    }



    public function add(Products $product){
    
        $product->qty = 1;
        return $product;
    }

    public function store(Request $request)
    {   
     
        $datos = $request->all();
        $datos['products'] =  json_decode($datos['products']);

        
        // Send email to user provider
        Mail::to( $datos['email'], $datos['full_name'])
                 ->send(new Proforma($datos));
                 
        // Sernd email to admin page
        Mail::to( 'admin@4gserver.com', '4gserver')
                 ->send(new ProformaCompany($datos));
        

        \Session::forget('cart');
        
        return redirect('notify/proforma-success');
    }


    public function trash(){

        \Session::forget('cart');
    }

}
