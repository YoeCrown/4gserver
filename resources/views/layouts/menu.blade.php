<!-- Header Starts -->
<div class="header">
    <!--=== Header v5 ===-->
    <div class="header-v5 header-static">
        <!-- Topbar v3 -->
        <div class="topbar-v3">
            <div class="search-open">
                <div class="container">
                    <input type="text" class="form-control" placeholder="Search">
                    <div class="search-close"><i class="icon-close"></i></div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <!-- Topbar Navigation -->
                        <form method="GET" action="#" accept-charset="UTF-8" id="headerSearchfrm" class="form-horizontal" name="headerSearchfrm">
                            <ul class="left-topbar">
                                <li><a>País (Colombia)</a>
                                    <ul class="currency">
                                        <li class="active">
                                            <a href="#">Colombia
                                                <i class="glyphicon glyphicon-ok"></i>
                                            </a>
                                        </li>
                                    <li class="">
                                        <a href="#">Ecuador

                                        </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <input type="text" class="" placeholder="Search" name="tag_search" id="tag_search" />
                                </li>
                            </ul><!--/end left-topbar-->
                        </form>
                    </div>
                    <div class="col-sm-6">
                        <ul class="list-inline right-topbar pull-right">
                            @if (Auth::guest())
                                <li><a href="{{ route('login') }}">Iniciar Sesion</a></li>
                                <li><a href="{{ route('register') }}">Registro</a></li>
                            @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>

                                        <li>
                                            <a href="{{ route('logout') }}"
                                                onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                                Logout
                                            </a>

                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                {{ csrf_field() }}
                                            </form>
                                        </li>
                                    </ul>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div><!--/container-->
        </div>
        <!-- End Topbar v3 -->
        <!-- Navbar -->
            <div class="navbar navbar-default mega-menu" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="{{url('')}}">
                            <img class="img-responsive" id="logo-header" src="{{asset('images/logoa.jpg')}}" alt="Logo">
                        </a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse navbar-responsive-collapse">
                        <!-- Shopping Cart -->
                            <ul class="list-inline shop-badge badge-lists badge-icons pull-right">
                            <li>
                                <a href="{{route('cart-show')}}">
                                    <span class="glyphicon glyphicon-shopping-cart tamaño"></span>
                                </a>
                                <span class="badge rounded-x" ng-class="itemList.length > 0 ? 'red' : 'badge-sea'">@{{ itemList.length }}</span>
                            </li>
                        </ul>
                        <!-- End Shopping Cart -->
                        <!-- Nav Menu -->
                        <ul class="nav navbar-nav">

                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="10">Servicios De Desbloqueos
                            <span class="caret"></span></button>  </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a tabindex="-1" href="{{route('jpsim')}}">JP-SIM </a></li>
                                    <li><a tabindex="-1" href="{{route('msl')}}">MSL</a></li>
                                    <li><a tabindex="-1" href="#">S7 Tmobile APP </a></li>
                                    <li><a tabindex="-1" href="#">Metropcs APP</a></li>
                                    <li><a tabindex="-1" href="#">ATT USA</a></li>
                                    <li><a tabindex="-1" href="#">SONY 0 Intentos</a></li>
                                </ul>
                            </li>
                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="10">Accesorios
                            <span class="caret"></span></button>  </a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a tabindex="-1" href="#">Cargadores </a></li>
                                    <li><a tabindex="-1" href="#">Manos Libres</a></li>
                                    <li><a tabindex="-1" href="#">Cables USB</a></li>
                                    <li><a tabindex="-1" href="#">Power Bank</a></li>
                                </ul>
                            </li>

                            <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="10">Herramientas Tecnico
                                <span class="caret"></span></button></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a tabindex="-1" href="#">Fuentes de voltaje</a></li>
                                    <li><a tabindex="-1" href="#">Fuentes de Calor</a></li>
                                    <li><a tabindex="-1" href="#">Planchas Termicas</a></li>
                                    <li><a tabindex="-1" href="#">Microscopios</a></li>
                                    <li class="dropdown1"><a>Stencils
                                        <span class="caret"></span></a>
                                        <ul class="dropdown1-content" role="menu">
                                            <li><a tabindex="-1" href="">Samsung S7</a></li>
                                            <li><a tabindex="-1" href=""> Iphone  5 </a></li>
                                            <li><a tabindex="-1" href=""> Samsung Note3 </a></li>
                                            <li><a tabindex="-1" href=""> Samsung Smartphone MTK </a></li>
                                            <li><a tabindex="-1" href=""> Samsung S6 </a></li>
                                            <li><a tabindex="-1" href=""> Iphone 6s </a></li>
                                            <li><a tabindex="-1" href=""> Iphone 6 </a></li>
                                            <li><a tabindex="-1" href=""> Samsung 5s </a></li>
                                            <li><a tabindex="-1" href=""> Iphone 6 Plus </a></li>
                                        </ul>
                                    </li>
                                    <li><a tabindex="-1" href="#">Herramientas</a></li>
                                    <li><a tabindex="-1" href="#">Consumibles</a></li>
                                    <li><a tabindex="-1" href="#">Solicitud Especial</a></li>
                                </ul>
                            </li>
                            <li><a href="{{route('products')}}">Productos</a></li>
                            <li><a href="{{route('pagos')}}">Pagos</a></li>
                        <!-- End Nav Menu -->
                    </div>
                </div>
            </div>
        <!-- End Navbar -->
    </div>
<!--=== End Header v5 ===-->
</div>