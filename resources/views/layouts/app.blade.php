<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>4gserver</title>

    <!-- Styles -->
        
         <link href="http://4gserver.com/images/chimera.jpg" rel="image_src" />

    
        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">
                <!-- CSS THEME -->
                <link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
                <link href="{{ asset('css/jquery-ui-1.10.3.custom.css') }}" rel="stylesheet">
                <link rel="stylesheet" href="{{ asset('css/shop.css') }}">
                <link rel="stylesheet" href="{{asset('css/header-v5.css')}}">
                <link rel="stylesheet" href="{{asset('css/footer-v1.css')}}">
                <link rel="stylesheet" href="{{asset('css/owl.carousel.css')}}">
                <link rel="stylesheet" href="{{asset('css/animate.css')}}">
                <link rel="stylesheet" href="{{asset('css/line-icons.css')}}">
                <link rel="stylesheet" href="{{asset('css/custom.css')}}">
        
        <link href="{{ asset('css/jquery.fancybox.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('css/jquery.mCustomScrollbar.css')}}">
        <link rel="stylesheet" href="{{asset('css/settings.css')}}">
        <!-- CSS Theme -->
        <link rel="stylesheet" href="{{asset('css/green.css')}}">
        <!-- CSS Customization -->


    <script src="{{ asset('js/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    <!-- JS Implementing Plugins -->
    <script src="{{ asset('js/smoothScroll.js') }}"></script>
    <script src="{{ asset('js/owl.carousel.js') }}"></script>
    <script src="{{ asset('js/jquery.mCustomScrollbar.concat.min.js') }}"></script>

    <!-- JS Page Level -->
    <script src="{{ asset('js/shop.app.js') }}"></script>
    <script src="{{ asset('js/owl-carousel.js') }}"></script>



        <!-- <script src="{{asset('datos.txt')}}"></script> -->
        <script src="{{ asset('js/angular.min.js') }}"></script>
        <script src="{{ asset('js/jquery.elevatezoom.js') }}"></script>
        <script src="{{ asset('js/sib/cart.js') }}"></script>
        <script src="{{ asset('js/sib/notify.min.js') }}"></script>
</head>
<body ng-app="cart" ng-controller="cartController">
    <div id="app">
        @include('layouts.menu')
        @yield('content')
        

   </div>

    <!-- Scripts -->
    <!-- Load javascripts at bottom, this will reduce page load time -->
  

    <script>
        $('.carousel').carousel({
            interval: 3000
        });
        $(document).ready(function() {
            App.init();
            App.initScrollBar();
            //App.initParallaxBg();
            OwlCarousel.initOwlCarousel();
            //RevolutionSlider.initRSfullWidth();
        });
    </script>






</body>
</html>
