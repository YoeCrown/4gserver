@extends('layouts.app')
@section('content')
    <div class="main">
        <div class="container">
            <div class="margin-bottom-40">                 
                <div class="content-page">
                    <div class="row">
                        <div class="col-md-12 blog-posts">
                            <div class="row">
                                <div class="principal">
                                            <h1>Es como abrir la billetera,<br><strong>pero en línea</strong></h1>
                                </div>
                                <div class="container sepa">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-xs-2">
                                                <img class="img-responsive ima" src="{{url('images/Bitcoin.png')}}" style="margin-top: 21px;">
                                            </div>
                                            <div class="col-xs-2">
                                                <img class="img-responsive ima" src="{{url('images/pichincha-logo.png')}}" style="margin-top: 10px; ">
                                            </div>
                                            <div class="col-xs-2">
                                                <img class="img-responsive ima" src="{{url('images/PayPal.svg.png')}}" style="margin-top: 15px; ">
                                            </div>
                                            <div class="col-xs-2">
                                                <img class="img-responsive ima" src="{{url('images/Skrill_primary_logo_RGB.svg.png')}}" style=" ">
                                            </div>
                                            <div class="col-xs-2">
                                                <img class="img-responsive ima" src="{{url('images/2000px-Logo_Bancolombia.svg.png')}}" style="margin-top: 10px;">
                                            </div>
                                            <div class="col-xs-2">
                                                <img class="img-responsive ima" src="{{url('images/efecty_transo.png')}}" style="margin-top: 10px;">
                                            </div>                         
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Products Lists Starts -->
                            <div id="js-product-view" class="product-list">
                                <div class="list-product-description product-description-brd margin-bottom-30">
                                    <div class="row">
                                        
                                        <div class="col-md-6">
                                            <a href="#"><img class="img-responsive sm-margin-bottom-20" src="images/pagos.png" alt=""></a>
                                            <a href="#"><img class="img-responsive sm-margin-bottom-20" src="images/formasdepago.png" style="width: 100%;"></a>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="valida">   
                                                <h2> PARA VALIDAR COMPRA</h2>
                                                <P>Para Hacer Efectiva Su compra Por favor 
                                                Hacer el Deposito o Transferencia  Recuerde el Costo del Envio para Quito GRATIS Para Evitar
                                                Entregas en persona solo enviamos por servientrega hasta su puerta.
                                                y el Resto del Pais es de 5 Dolares.  Puede Enviarme Una Foto del Deposito 
                                                o Transferencia al numero:<br>
                                                <strong>WHATSAPP! :  0984217755</strong> o <strong>Para LLamadas: 0984217755</strong></P>
                                            </div>
                                            <div class="valida">   
                                                <h2>¿Puedo Pasar Retirando el Producto?</h2>
                                                <P>SI CON GUSTO Puede hacerlo Con la Condición que nos informe con Exactitud la hora Exacta
                                                 en la que pasara por el producto sin algún tipo de excusa. (Por ejemplo:  Un ratito,
                                                 Aguánteme , Vea Disculpe se me Presento un Inconveniente, Mas chancecito.)  Si usted 
                                                acuerda una hora Determinada es la hora en la que se encontrara en la dirección Pactada
                                                 Sin ningún tipo de Excusa por favor.<br>
                                                 La Direccion Es AMERICA Y CUERO y Caicedo una Vez Pactada una hora Exacta Llamar Cuando Se encuentre en el punto para Salir a Entregarle su Producto.
                                                 </P>

                                            </div>
                                            <div class="col-md-6">
                                                <div class="valida">   
                                                    <h2>RECUERDE !</h2>
                                                    <p>Recuerda Enviar Via Whatsapp  0984217755 Tu foto de deposito o Transferencia
                                                    El costo del Envio EN QUITO :<strong> 3 Dolar</strong> Resto del pais:<strong> 5 dolar</strong>
                                                    ADJUNTA LOS SIGUIENTES DATOS PARA EL ENVIO</strong>
                                                    <strong>NOMBRE - CEDULA - CELULAR - DIRECCION - CIUDAD - PROVINCIA - TIPO DE ENVIO:</strong> Servientrega o Transporte.</p>
                                                </div>                                         
                                            </div>
                                            <div class="col-md-6">
                                                <div class="valida">   
                                                    <h2>ENVIOS TODOS LOS DIAS</h2>
                                                    <p>En Dos Ocaciones :<strong> 11:00 Am , 5:00 Pm</strong><br>
                                                    Tiempo De entrega Normalmente Servientrega <strong> al Siguiente Dia Desde las 10Am.</strong>
                                                    Una Vez Enviado Sera Enviada la Foto de la Guia Via Whatsapp Donde Usted Hara Seguimiento a Su compra en Tiempo Real.</p>
                                                </div>                                         
                                            </div>
                                        </div>                                             
                                   </div>
                                </div>
                            </div>
                        </div>                                              <!-- Products Lists Ends -->                            
                    </div>
                </div>  <!-- Sidebar Ends -->
            </div>
        </div>
    </div>
        
@endsection
