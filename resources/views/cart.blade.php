@extends('layouts.app')

@section('content')

<div class="container">
    <div class="margin-bottom-40">                
        <div class="alert alert-success">
            <h3 class="no-margin">
                <strong>@{{itemList.length}}</strong>
                <span>Articulos en su Compra</span>
                <button type="button" name="remove_all" value="remove_all" class="btn red-sunglo btn-xs margin-left-5" ng-click="trash()"><i class="glyphicon glyphicon-trash"></i> Vaciar Carrito</button>
            </h3>
        </div>
        <!-- Alert Block Starts -->
<!--         <div class="alert alert-success">
            Añadido Satisfactoriamente a su Carrito de Compra.
        </div> -->
        <!-- Alert Block Ends -->
        <div class="clearfix form-horizontal">
            <div class="form-group fn_clsPriceFields ctrl-labelleft margin-bottom-30 ">
                <label for="shipping_country" class="col-md-2 control-label required-icon">Enviar pedido a:</label>
                    <div class="col-md-2">
                        <select class="form-control select2me input-medium fnShippingCostEstimate" id="shipping_country" name="shipping_country">
                            <option value="65" selected="selected">Colombia</option>
                            <option value="82">Ecuador</option>
                        </select>
                        <label class="error"></label>
                    </div>
            </div>
        </div>
        <small id="fn_dialog_confirm_msg" class="confirm-delete" style="display:none;"></small>
        <div class="responsive-view">
           <!--      <center style="margin-bottom:2eM;" ng-show="itemList.length">
                <buttom ng-click="trash()" class="btn btn-danger">
                     Empty Cart <i class="glyphicon glyphicon-trash"></i>
                </buttom>
                </center> -->
                <div class="responsive-xscroll">
                    <form method="POST" action="#" accept-charset="UTF-8" id="itemFrm_46" class="form-horizontal cart-list">
                        <input name="_token" type="hidden" value="2CgWXnKm7Ulv5XWpnIy0vbJPVP9RuUWRZGNAjsQt">
                        <input id="itemdetails_list_owner_46" name="itemdetails_list_owner_46" type="hidden" value="46">
                        <div class="blog-item">
                            <div class="cart-list-title text-center  ">
                                <div class="row hidden-sm hidden-xs">
                                    <div class="col-md-5"><strong>Articulo</strong></div>
                                    <!--<div class="portfolio-info col-md-5"><strong>Price</strong></div>-->
                                    <div class="col-md-1"><strong>Cantidad</strong></div>
                                    <div class="col-md-1"><strong>Precio</strong></div>
                                    <div class="col-md-1"><strong>Iva</strong></div>
                                    <div class="col-md-1"><strong>Sub total</strong></div>
                                    <div class="col-md-1"><strong>Eliminar</strong></div>
                                </div>
                            </div>
                        </div>
                        {{--  Cart item session  --}}
                        <div class="blog-item">
                            <div class="portfolio-block text-center">
                        

                                <div class="row" ng-repeat="item in itemList track by $index">
                                    <div class="col-md-5 portfolio-info">
                                        <div class="portfolio-text">
                                            <a href="#"><img id="item_thumb_image_id" 
                                            ng-src="{{asset('upload')}}/@{{item.name_image}}"  width='200' height='160'  title="Gpg Magma Box" alt="Gpg Magma Box"  class="media-object"/></a>
                                            <div class="portfolio-text-info">
                                                <h4><a href="">@{{item.item}}</a></h4>
                                                <p>Product Code: <span class="text-muted">@{{item.code}}</span></p>
                                            </div>
                                        </div>
                                    </div>
                                    <!--<div class="col-md-4 col-sm-4 col-xs-4 portfolio-info"><small>USD</small> <strong>140</strong></div>-->
                                    <div class="col-sm-4 hidden-md hidden-lg"><b>Qty</b>
                                    </div>
                                    <div class="col-md-1 col-sm-32 portfolio-info">
                                        <input
                                          type="number"
                                          ng-model="item.qty"
                                          class="form-control"
                                          ng-disabled="updateStatus"
                                          onkeypress='return event.charCode >= 48 && event.charCode <= 57' required
                                          >
                                          <!-- ng-keyup="updateQty();" -->

                                          <!-- ng-change="updateQty()" -->
                                        
                                    </div>
                                    <div class="col-sm-3 hidden-md hidden-lg"><b>Price</b>
                                    </div>
                                    <div class="col-md-1 col-sm-3 portfolio-info">
                                        <span id="product_total_76">
                                            <small>USD</small> <strong>@{{item.price}}</strong>
                                            </span>
                                    </div>
                                    <div class="col-md-1 hidden-xs hidden-sm portfolio-info">
                                        <strong class="badge badge-primary">@{{item.tax}}</strong>
                                    </div>
                                    <div class="col-sm-4 hidden-md hidden-lg "><b>Subtotal</b>
                                    </div>
                                    <div class="col-md-1 col-sm-4 portfolio-info">
                                        <span id="new_product_subtotal_76">
                                         USD @{{ item.qty * item.price }}
                                        </span>
                                    </div>
                                    <div class="col-sm-4 hidden-md hidden-lg clearview"><b>Remove</b>
                                    </div>
                                    <div class="col-md-1 col-sm-4 cart_product_remove portfolio-info">
                                        <div class="action-btn">
                                            {{--  <a href="{{route('cart-delete',$item->code)}}"  title="Remove from cart" class="btn  btn-danger"><i class="glyphicon glyphicon-remove"></i></a>  --}}
                                            <buttom class=" btn btn-danger btn-md" ng-click="deleteItem($index)"  ng-disabled="updateStatus" > <i class="glyphicon glyphicon-trash "></i> </buttom>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                        {{--  end cart  --}}
                    </form>
                </div>

                <center ng-show="!itemList.length"><h3><span class="label label-warning">No hay productos en el carrito :(</span></h3></center>
        </div>
            
            

            <div class="clearfix" style="text-align: right;">
                <p class="no-margin fonts18">Total de su Compra: <span id="checkout_total"><small>USD</small> <strong>@{{totalAmount()}}</strong></span></p>
            </div>

        <div class="clearfix">
         <!-- Button trigger modal -->
            <buttom class="btn btn-lg btn-success pull-right" data-toggle="modal" data-target="#myModal" ng-show="itemList.length"><i class="glyphicon glyphicon-ok"></i> Procesar pedido</buttom>
        </div>

        <br>

    </div>
        @include('layouts.noticias')
</div>
</div>

    @include('layouts.subscribe')
    @include('layouts.contact')


@endsection

<!-- <script>
    function realizaProceso(item ){
        $.ajax({

                url:   '/cart/delete/'+item,
                type:  'GET',
                success:  function (response) {
                    console.log(response);
                }
        });
    }
</script> -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">
            <span class="glyphicon glyphicon-shopping-cart tamaño"></span> 
            <b>COMPLETE SU ORDEN</b> 
        </h4>
      </div>
      <div class="modal-body">
       <form action="{{route('cart-proforma')}}" class="form-inline" method="POST">
             {{ csrf_field() }}
                <div class="row">
                        
                    
                        <div class="col-sm-12 col-lg-6">
                            <h3>Nombre Completo</h3>
                            <div class="form-group">
                            <input type="text" class="form-control"  name="full_name" placeholder="Nombre Completo" required>
                            </div>
                        </div>
                        <div class="col-sm-12 col-lg-6">
                            <h3>Telefono</h3>
                            <div class="form-group">
                                <input type="tel"  class="form-control" name="phone"  placeholder="Telefono" required>
                            </div>
                        </div>          
                
                        <div class="col-sm-12 col-lg-6">
                          <h3>Correo Electronico</h3>
                          <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="Correo Electronico" required>
                          </div>
                        </div>

                
                    <div class="col-sm-12 col-lg-6">
                        <h3>Metodos de pago:</h3>
                        <div class="form-group">
                            <select class="form-control" name="method_pay" required style="width: 100%;display: block;">
                                <option value="Paypal">Paypal</option>
                                <option value="Tranference">Transference  Por  Banco Pinchicha</option>
                                <option value="Skrill">Skrill </option>
                                <option value="Bancolombia">Bancolombia </option>
                                <option value="Bitcoin">Bitcoin </option>
                            </select>
                        </div> 
                    </div>

                    <div class="col-sm-12 col-lg-6">
                        <div class="form-group">
                        <br>
                        <br>
                        <label for="inputEmail3" class=" control-label" style="font-size: 22px"><b>Monto a pagar : </b> $@{{totalAmount()}}</label>
                        <input type="hidden" class="form-control"  name="total_amount" value="@{{totalAmount()}}">
                        </div>
                    </div>

                    <div class="col-sm-12 col-lg-6">
                        <div class="form-group">
                        <br>
                        <br>
                        
                            <button type="button" class="btn btn-danger" data-dismiss="modal" style="margin-right: 20px">
                            <span class="glyphicon glyphicon-remove"></span>
                            Cerrar
                            </button>

                            <button type="submit" class="btn btn-primary">
                            <span class="glyphicon glyphicon-ok"></span>
                            Enviar
                            </button>

                    </div>
                    </div>

                     
              </div>

              <div class="row">
                  <div class="col-lg-12">
                    <style type="text/css">
                        #notas-checkout li{margin-bottom: 3px;}
                    </style>
                    <ol style="margin-top: 0px;padding: 10px;font-size: 11px" id="notas-checkout">
                        <li> Servientrega no recibe paquetes que no tengan los datos completos.</li>
                        <li>Obligacion Entregar Todos Estos Datos En Ese Orden Nombres Completos por favor y datos completos</li>
                        <li><b>ENVIOS TODOS LOS DIAS</b>  En Dos Ocaciones :  11:00 Am , 5:00 Pm</li>
                        <li><b>COSTO DE ENVIO </b> para QUITO  3 Dolares , Resto del Pais 5 Dolares   por Servientrega o Transporte es el Mismo precio.</li>
                        <li>Tiempo De entrega Normalmente Servientrega al Siguiente Dia Desde las 10Am.</li>
                        <li>Una Vez Enviado Sera Enviada la Foto de la Guia Via Whatsapp Donde Usted Hara Seguimiento a Su compra en Tiempo Real.</li>
                    </ol>
                  </div>
              </div>


            <div class="form-group" style="display: none">
                <div class="col-sm-12">
                 <input type="hidden" class="form-control" name="products" id="products" value="@{{itemList}}">
                    <h3>Products in Order</h3>
                    <div ng-repeat="item in itemList track by $index">
                        <div class="form-group" >
                            
                            <label for="inputEmail3" class=" control-label"> - @{{item.code}} @{{item.item}} @{{item.qty}} </label>
                           
                        </div>
                    </div>
                </div>
            </div>          
           

        </form>
    </div>
  </div>
</div>
</div>
