<?php

namespace App\Http\Controllers;

use App\Feature;
use App\Products;
use App\Products_images;
use Illuminate\Http\Request;

class DetalleproductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $product = Products::findOrFail($id);

        $features = Feature::where('code_id', $id)->get();

        $images = Products_images::where('product_code',$id)->get();
        
        $imageFirst = Products_images::where('product_code',$id)->first();
      
        return view('detalleproducto', compact('product','features','images','imageFirst'));
    }


}