@extends('layouts.app')
@section('content')
    <div class="main">
            <div class="container">
                <div class="margin-bottom-40">

                    <!--=== Slider ===-->
                        <div class="carousel slide carousel-fade carousel-v1 margin-bottom-40" id="myCarousel-2" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <a href="{{route('msl')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/bannerdesbloqueos8.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/jpsimbanner.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/cargador.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/fuente.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/Herramientas.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/STENCIL-IPHONE-6PLUS.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/Medidor.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/STENCIL-IPHONE-6.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/Pegamento.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                 <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/STENCIL-SAMSUNG-S6.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/Reactivador.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/stencil-iphone-5.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>


                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/STENCIL-IPHONE-6S.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/STENCIL-NOTE-3.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/STENCIL-NOTE-4.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/STENCIL-S6.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/STENCIL-S7.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/STENCIL-SAMSUNG-MTK.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/TESTER-POTENCIA.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                                <div class="item">
                                    <a href="{{route('jpsim')}}">
                                    <img alt="Chimera Dongle Authenticator" src="{{asset('images/Banners/STENCIL-SAMSUNG-S5.jpg')}}" />
                                    </a>
                                    <div class="carousel-caption">

                                    </div>
                                </div>
                            </div>
                            <div class="carousel-arrow">
                                <a data-slide="prev" href="#myCarousel-2" class="left carousel-control">
                                    <i class="glyphicon glyphicon-arrow-left"></i>
                                </a>
                                <a data-slide="next" href="#myCarousel-2" class="right carousel-control">
                                    <i class="glyphicon glyphicon-arrow-right"></i>
                                </a>
                            </div>
                        </div>
                        <!-- End  Slider -->
                        <div class="headline"><h2>Lista de Productos</h2></div>
                        <!--=== Illustration v2 ===-->
                        <div class="illustration-v2 margin-bottom-60">
                            <div class="customNavigation margin-bottom-25">
                                <a class="owl-btn prev rounded-x"><i class="glyphicon glyphicon-arrow-left"></i></a>
                                <a class="owl-btn next rounded-x"><i class="glyphicon glyphicon-arrow-right"></i></a>
                            </div>
                            <ul class="list-inline owl-slider">
                                 @foreach ($products as $product)
                                <!--=== comienza 1 ===-->
                                <li class="item">
                                    <div class="product-img ">
                                        <a href="#">
                                        <img class="img-responsive index-blk-img center-block" src="{{ asset('upload/'.$product->name_image) }}" alt=""></a>
                                        <a class="product-review" href="{{route('detalleproducto', $product->code)}}">Mas Detalles</a>
                                        <a class="add-to-cart" href="#" ng-click="addItem({{$product->code}});" onclick="return false;" ><i class="glyphicon glyphicon-shopping-cart"></i>Agregar a Compras</a>
                                    </div>
                                    <div class="product-description product-description-brd">
                                        <div class="overflow-h margin-bottom-5">
                                            <div class="">
                                                <span class="title-price hideOverflow"><a href="{{route('detalleproducto', $product->code)}}">{{$product->item}}</a></span>
                                                <span class="gender">{{$product->description}}</span>
                                            </div>
                                            <div>
                                                <span class="title-price">${{$product->price}}</span>
                                                <span class="title-price line-through pull-right "></span>
                                            </div>
                                        </div>
                                        <ul class="list-inline product-ratings">
                                            <li class="like-icon pull-left"><a ng-click="addItem({{$product->code}});" onclick="return false;" data-original-title="Agregar a Compras" data-toggle="tooltip" data-placement="right" class="fn-add-cart tooltips" href="javascript:;" onClick="jsAddToCart('76')" ><i class="glyphicon glyphicon-shopping-cart"></i></a></li>
                                            <li class="like-icon pull-right"><a data-original-title= 'Agregar Lista de Deseos' data-toggle="tooltip" data-placement="left" data-productid="76" class="fn_signuppop tooltips" href="#"><i class="glyphicon glyphicon-star"></i></a></li>
                                        </ul>
                                    </div>
                                </li>
                                <!--=== Termina  1 ===-->
                                @endforeach

                            </ul>
                        </div>

                        <!--=== End Illustration v2 ===-->
                        <div class="headline"><h2>NUEVO STOCK</h2></div>

                        <!--=== Illustration v2 ===-->

                    <!--=== Comienza1 ===-->
                        <div class="row illustration-v2">
                         @foreach ($latest as $item)
                            <div class="col-md-3 col-sm-6 md-margin-bottom-30">
                                <div class="product-img ">
                                    <a href="#">

                                        <img class="img-responsive index-blk-img center-block"
                                        src="{{ asset('upload/'.$item['name_image']) }}" alt=""></a>

                                    <a class="product-review" href="#">Ver Vistazo Rapido</a>

                                    <a class="add-to-cart" href="#" ng-click="addItem({{$item['code']}});" onclick="return false;">
                                        <i class="glyphicon glyphicon-shopping-cart"></i>
                                        Agregar a Compras
                                    </a>
                                </div>

                                <div class="product-description product-description-brd margin-bottom-15">
                                    <div class="overflow-h margin-bottom-5">
                                        <div class="">
                                            <span class="title-price hideOverflow"><a href="#">{{ $item['item'] }} </a></span>
                                            <span class="gender">Accesorios para Celular</span>
                                            <span class="gender">&nbsp</span>
                                        </div>
                                        <div>
                                            <span class="title-price">${{ $item['price'] }}</span>
                                            <span class="title-price line-through pull-right "></span>
                                        </div>
                                    </div>
                                    <ul class="list-inline product-ratings">
                                        <li class="like-icon pull-left">
                                            <a data-original-title="Agregar a Compras" class="fn-add-cart tooltips" href="" >
                                                <i class="glyphicon glyphicon-shopping-cart"></i>
                                            </a>
                                        </li>
                                        <li class="like-icon pull-right">
                                            <a data-original-title= 'Agregar Lista de Deseos'
                                            data-toggle="tooltip" data-placement="left" data-productid="256" class="fn_signuppop tooltips" href="#">
                                                <i class="glyphicon glyphicon-star"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        @endforeach
                            <!--=== Termina1 ===-->



                        </div>




                @include('layouts.noticias')

               </div>
            </div>
    </div>

    @include('layouts.subscribe')
    @include('layouts.contact')

@endsection
