<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products_images extends Model
{
       
    protected $table ='product_images';

    protected $fillable = ['name','path','product_code'];
    

}
