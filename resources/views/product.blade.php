@extends('layouts.app')

@section('content')
        <div class="main">
            <div class="container">
                <div class="margin-bottom-40">
                	<div class="result-category">
                   	 <h2>Items </h2>
                		<small class="shop-bg-red badge-results">(147) Results</small>
                	</div>
	               <hr class="hr-xs">
                    <div class="content-page">
                        <div class="row">
                            <div class="col-md-9 blog-posts">
                                <div id="error_msg_div"></div>
                                <!-- Alert Block Starts -->
                                                <!-- Alert Block Ends -->
                                <!-- Tab Menu Starts -->
                                <div class="tab-menu margin-bottom-20">
                                	<!-- <a href="" class="btn purple-plum"><i class="fa fa-list margin-right-5"></i> Menu</a> -->
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#">Recently Added</a></li>
                                        <li><a href="#">Most Viewed</a></li>
                                        <li><a href="#">Most Sold</a></li>
                                        <li><a href="#">Free</a></li>
                                        <li><a href="#">Featured</a></li>
                                    </ul>
                                </div>
                                <!-- Tab Menu Ends -->
                            <!-- Products Lists starts -->

                            @foreach ($products as $product)
                                <div class="list-product-description product-description-brd margin-bottom-30">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <a href="#"><img class="img-responsive sm-margin-bottom-20" src="{{asset('upload/'.$product->name_image)}}" alt=""></a>
                                        </div>
                                            <!-- <div class="product-description col-md-10 col-sm-10 col-xs-10"> -->
                                                <div class="col-sm-8 product-description">
                                                <h4 class="title-price"><a href="{{route('detalleproducto', $product->code)}}" title="{{$product->item}}">{{$product->item}}</a></h4>
                                                <ul class="list-inline add-to-wishlist">
                                                    <li class="wishlist-in"><a href="#" class="fn_signuppop" action="#"><i class="glyphicon glyphicon-heart"></i>Add to wishlist</a></li>
                                                    <li>
                                                        <div class="clearfix">
                                                            <span class="pull-left">Category:</span>
                                                            <span class="product-category"><a href="#">Flash &amp; Unlock</a> / <a href="#">Cables and Adapters</a> / <a href="#">Cables</a></span>
                                                        </div>
                                                    </li>
                                                <div class="clearfix price-product">
                                                    <div class="special-price">
                                                        <ul class="list-inline">
                                                            <li>
                                                                <strong>{{$product->price}}$</strong>
                                                            </li>
                                                        </ul>
                                                    </div>

                                                    <div class="price-cart pull-right">
                                                        <form method="POST" action="#" accept-charset="UTF-8" class="form-horizontal no-margin" id="addCartfrm" name="addCartfrm"><input name="" type="hidden" value="">
                                                            <input id="product_id" name="product_id" type="hidden" value="265">
                                                            <input qty="qty" name="qty" type="hidden" value="1">
                                                            <a href="#" name="add_to_cart" ng-click="addItem({{$product->code}});" class="btn-u  pull-right responsive-btn-block"><i class="glyphicon glyphicon-shopping-cart margin-right-5"></i> Add To Cart</a>
                                                            <div class="pos-relative">
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                            @endforeach

                            <!-- Products Lists Ends -->

                            <div class="text-right">
                                    <div class="mb15 text-right">
                                        {{ $products->links() }}
                                    </div>

                                </div>
                            </div>

                            <!-- Sidebar Starts -->
                            @if(Auth::user() && Auth::user()->admin() == 1 )
                            <div class="col-md-3">
                               <form action="{{route('importar')}}" enctype="multipart/form-data" method="POST">
                                       {{ csrf_field() }}

                                <input id="archivo" accept=".csv" name="archivo" type="file" required="" />
                                <input name="MAX_FILE_SIZE" type="hidden" value="20000" /> <br>
                                <input class="btn btn-success" name="enviar" type="submit" value="Importar Productos" />
                                </form>
                                <br><br>
                            </div>
                            @endif
                            <div class="col-md-3 blog-sidebar index-well">
                                <!-- Categories Block Starts -->
                                <div class="panel-group">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h2 class="panel-title">
                                                Categories
                                            </h2>
                                        </div>
                                        <div class="panel-body">
                                            <ul class="nav list-unstyled">
                                                <li><a href="#" title="Flash &amp; Unlock (128)">Flash &amp; Unlock (128)</a></li>
                                                <li><a href="#" title="Cell Phones Accessories (6)">Cell Phones Accessories (6)</a></li>
                                                <li><a href="#" title="Spare Parts &amp; Components  (0)">Spare Parts &amp; Components  (0)</a></li>
                                                <li><a href="#" title="Repair Equipment (13)">Repair Equipment (13)</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- Categories Block Ends -->
                                    <!-- Refine Search Block Starts -->
                                    <div class="filter-by-block md-margin-bottom-60">
                                        <h1>Filter By</h1>
                                    </div>
                                    <form method="GET" action="#" accept-charset="UTF-8" class="form-horizontal" id="" name="productSearchfrm">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h2 class="panel-title">
                                                    Categories
                                                </h2>
                                            </div>
                                            <div class="panel-body">
                                                <ul class="list-unstyled checkbox-list">
                                                    <label class="checkbox">
                                                    <input id="2" name="cat_search[]" type="checkbox" value="2"> <i></i>Flash &amp; Unlock </label>
                                                    <label class="checkbox">
                                                    <input id="18" name="cat_search[]" type="checkbox" value="18"> <i></i>Cell Phones Accessories </label>
                                                    <label class="checkbox">
                                                    <input id="20" name="cat_search[]" type="checkbox" value="20"> <i></i>Spare Parts &amp; Components  </label>
                                                    <label class="checkbox">
                                                    <input id="34" name="cat_search[]" type="checkbox" value="34"> <i></i>Repair Equipment </label>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h2 class="panel-title">
                                                    Keywords
                                                </h2>
                                            </div>
                                            <div class="panel-body">
                                                <input class="form-control" name="tag_search" type="text">
                                            </div>
                                        </div>
                                    <!-- Price Range Block Starts -->
                                    </form>
                                </div>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h2 class="panel-title">
                                            Price Range
                                        </h2>
                                    </div>
                                    <div class="panel-body">
                                        <div class="form-group margin-bottom-45">
                                            <div class="col-md-4 col-sm-2 col-xs-3">
                                                <label for="product_qty" class="control-label">Quantity</label>
                                            </div>
                                            <div class="col-md-8 col-sm-10 col-xs-9"><input class="form-control" id="product_qty" name="product_qty" type="text"></div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-2 col-sm-2 col-xs-3">
                                                <label for="price_range_start" class="control-label">USD</label>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-3 padding-right-0">
                                                <input class="form-control" id="price_range_start" name="price_range_start" type="text">
                                            </div>
                                            <div class="col-md-2 col-sm-1 col-xs-2 text-right">
                                                <label for="price_range_end" class="control-label">to</label>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-3 padding-left-0">
                                                <input class="form-control" id="price_range_end" name="price_range_end" type="text">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- Price Range Block Ends -->
                                <div class="sidebar-btn">
                                    <button type="submit" name="search_products" value="search_products" class="btn-u btn-brd-hover"><i class="glyphicon glyphicon-search"></i> Search</button>
                                    <button type="reset" name="reset_products" value="reset_products" class="btn" onclick="return clearForm(this.form);"><i class="glyphicon glyphicon-repeat"></i> Reset</button>
                                </div>
                                <!-- Refine Search Block Ends -->
                            </div>
                        </div>            <!-- Sidebar Ends -->
                    </div>
                </div>

            </div>
        </div>
@endsection