@extends('layouts.app')
@section('content')
    <div class="main">
            <div class="container">
                <div class="margin-bottom-40">
                	
	               <hr class="hr-xs">
                    <div class="content-page">
                        <div class="row">
                            <div class="col-md-12 blog-posts">
                                <!-- Products Lists Starts -->
                                <div id="js-product-view" class="product-list">
                                    <div class="list-product-description product-description-brd margin-bottom-30">
                                        <div class="row">
                                            <div class="col-sm-4">
                								<a href="#"><img class="img-responsive sm-margin-bottom-20" src="images/jpsim.png" alt=""></a><br>
                                                <a href="#"><img class="img-responsive sm-margin-bottom-20" src="images/jpsim2.png" alt=""></a><br>
                                                <a href="#"><img class="img-responsive sm-margin-bottom-20" src="images/jpsim3.png" alt=""></a>
                							</div>
                                                <!-- <div class="product-description col-md-10 col-sm-10 col-xs-10"> -->
                								<center><p class="title-price" style="color:red;font-size: 80px;"><strong>JP SIM  4G LTE</strong></p></center>
                                                <center><h2 style="color: black;">Únicas Con <span style="color: red;font-size: 39px;">32</span> Mbps</h2></center>
                                                    <div class="col-sm-8 product-description">

                                                        <h1 style="color: red;margin-top: 100px;">Precios:</h1>
                                                        <ul style="font-size: 40px; ">
                                                            <li><p style="color: black; font-size: 40px;"><span style="color: #3B66CB;">1</span> unidad = <span style="color: #3B66CB;">30</span> Dólares</p></li>
                                                            <li><p style="color: black; font-size: 40px;margin-top: -40px;"><span style="color: #3B66CB;">3   </span> unidad = <span style="color: #3B66CB;">70</span> Dólares</p></li>
                                                            <li><p style="color: black; font-size: 40px;margin-top: -40px;"><span style="color: #3B66CB;">5</span> unidad = <span style="color: #3B66CB;">100</span> Dólares</p></li>
                                                            <li><p style="color: black; font-size: 40px;margin-top: -40px;"><span style="color: #3B66CB;">10</span> unidad = <span style="color: #3B66CB;">170</span> Dólares</p></li>
                                                            <li><p style="color: black; font-size: 40px;margin-top: -40px;"><span style="color: #3B66CB;">20</span> unidad = <span style="color: #3B66CB;">300</span> Dólares</p></li>
                                                            <li><p style="color: black; font-size: 40px;margin-top: -40px;"><span style="color: #3B66CB;">50</span> unidad = <span style="color: #3B66CB;">500</span> Dólares</p></li>
                                                        </ul>
                                                        <p style="color: red;font-size: 50px;">Características: </p>
                                                        <ul style="font-size: 30px;">
                                                            <li><p style="color: black;font-size: 22px;">Funciona Con Todos Los IPhone  5 , 5c ,5s , 6 , 6s , 6 Plus, 7 , 7 Plus.</p></li>
                                                            <li><p style="color: black;font-size: 22px;margin-top: -50px;">Funciona Con Todos los Sistemas Operativos IOS 10.3.3, 11.0 </p></li>
                                                            <li><p style="color: black;font-size: 22px;margin-top: -50px;">Funciona con todas las operadoras incluidos los teléfonos con deuda en el exterior o stolen para cobrar el seguro,  todas las compañías del Mundo.</p></li>
                                                            <li><p style="color: black;font-size: 22px;">Funciona en LTE , 4G alcanzando velocidades Extremas en Videos realizados en Vivo, Esta Característica es Única de las JP SIM desarrolladas con Scripts Locales APNS Manuales de las Redes  Según Posición Geográfica desarrollado por Nosotros Mismos.</p></li>
                                                            <li><p style="color: black;font-size: 22px;">No se Pierde La Señal, No Se pierden Las Llamadas, No se Pierde La Conexión. Nuestra Script Busca y encuentra Automáticamente La intensidad de cada Antena y se conecta a Ella. ES IMPOSIBLE QUE LA COMPETENCIA LE OFREZA EL MISMO PRODUCTO.</p></li>
                                                        </ul>
                                                    
                                                    </div>
                                            </div>
                					   </div>
                     			    </div>
                                                <!-- Products Lists Ends -->                            
                                </div>
                            </div>            <!-- Sidebar Ends -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
