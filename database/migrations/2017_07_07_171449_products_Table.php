<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('code');
            $table->string('item')->default('undefined');
            $table->string('url_image')->default('undefined');
            $table->string('name_image')->default('undefined');
            $table->text('description');
            $table->integer('qty')->default(0);
            $table->float('price')->default(0);
            $table->float('tax')->default(0);
            $table->float('precio_compra')->default(0);
            $table->float('ganancia')->default(0);
            $table->float('precio_colombia')->default(0);
            $table->string('proveedor')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');

    }
}
