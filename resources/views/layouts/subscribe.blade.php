<div class="shop-subscribe">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 md-margin-bottom-20">
                        <h2>Suscribete a nuestras  <strong>Noticias</strong></h2>
                    </div>
                    <div class="col-md-4">
                        <form method="POST" action="#" accept-charset="UTF-8" id="newsletterFrm" class="ng-pristine ng-valid"><input name="_token" value="#" autocomplete="off" type="hidden">
                        <div class="input-group">
                            <input class="form-control" name="email" id="email" placeholder="Escribe tu correo aqui..." type="text">
                                <span class="input-group-btn">
                                    <button class="btn" type="submit"><i class="glyphicon glyphicon-envelope"></i></button>
                                </span>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
    </div>