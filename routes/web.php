<?php

use App\Products;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/welcome', function () {
//     return view('welcome');
// });


Route::get('/', function () {

     $products = Products::all();
	 $latest = new Products;
     $latest = $latest->orderby('updated_at','DESC')
            ->take(4)
            ->get()->toArray();

    return view('index', compact('products','latest'));

})->name('index');




Route::get('productos','ProductController@index')->name('products');;

Route::post('importar', 'ImportController@import')->name('importar');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('notify/register-success','Notify@register_success');
Route::get('notify/csv-import-success','Notify@csv_import_success');
Route::get('notify/proforma-success','Notify@proforma_success');


Route::get('msl','MslController@index')->name('msl');
Route::get('jpsim','JpsimController@index')->name('jpsim');
Route::get('pagos','PagosController@index')->name('pagos');

// Route::get('detalleproducto/{product}','DetalleproductoController@index')->name('detalleproducto');

Route::get('detalleproducto/{product?}', [
     'as' => 'detalleproducto',
     'uses' => 'DetalleproductoController@index'
]);
//CART

Route::get('/cart/show', [
     'as' => 'cart-show',
     'uses' => 'CartController@show'
]);

Route::get('/cart/add/{product}', [
     'as' => 'cart-add',
     'uses' => 'CartController@add'
]);

Route::get('/cart/delete/{product}', [
     'as' => 'cart-delete',
     'uses' => 'CartController@delete'
]);

Route::get('/cart/trash', [
     'as' => 'cart-trash',
     'uses' => 'CartController@trash'
]);


Route::get('/cart/update/{product}/{qty?}',[
     'as' => 'cart-update',
     'uses' => 'CartController@update'
]);

Route::get('/cartJson',[
     'as' => 'cart-json',
     'uses' => 'CartController@cartJson'
]);

Route::post('/cartUpdateJson',[
     'as' => 'cart-udate-json',
     'uses' => 'CartController@cartUpdateJson'
]);


Route::post('/cart/proforma',[
     'as' => 'cart-proforma',
     'uses' => 'CartController@store'
]);


Route::get('hambre', function () {


    return view('hambre');

});

Route::post('publish', 'PublishController@publish')->name('publish');
Route::delete('publishDelete/{id?}', 'PublishController@destroy')->name('publishDelete');
