@extends('layouts.app')



@section('content')

<div class="container" style="min-height: 450px">
	<div class="row">
		<div class="col s12">
			<div style="text-align: center">
				

				<div id="loader"></div>

				<div id="myDiv" class="animate-bottom">
				  <h2 style="margin-top: 3em;">Actualizacion satisfactoria</h2>
				  <span style="font-size: 3em" class="glyphicon glyphicon-ok" aria-hidden="true"></span>
				  <p>Se han actualizado los elementos de la tienda correctamente</p>
				  <a href="{{url('productos')}}" class="btn btn-success">Volver a la lista de productos</a>

				  <br>

				</div>

			</div>
				<!-- <script>

				$( document ).ready(function() {
					myFunction();
				});

				var myVar;

				function myFunction() {
				    myVar = setTimeout(showPage, 3000);
				}

				function showPage() {
				  document.getElementById("loader").style.display = "none";
				  document.getElementById("myDiv").style.display = "block";
				}
				</script> -->



        </div>
	</div>
</div>



@endsection