<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
     protected $fillable = ['code_id', 'detail', 'video','features',];

}
