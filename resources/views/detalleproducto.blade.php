@extends('layouts.app')
@section('content')
    <div class="main">
            <div class="container">
                <div class="margin-bottom-40">

                   <hr class="hr-xs">
                    <div class="content-page">
                        <div class="row">
                            <div class="col-md-12 blog-posts">
                                <!-- Products Lists Starts -->
                                <div id="js-product-view" class="product-list">
                                    <div class="list-product-description product-description-brd margin-bottom-30">
                                        <div class="row">
                                           <div class="col-sm-5">

                                                  @if($images->isEmpty())
                                                     <a href="#"><img class="img-responsive sm-margin-bottom-20" src="{{asset('upload')}}/{{$product->name_image}}" alt=""></a><br>          
                                                  @endif 
                                                

                                                  @if($images->isNotEmpty())
                                                    
                                                      
                                                     <img class="img-responsive sm-margin-bottom-20" id="zoom_03"
                                                          src="{{url($images[0]->path.'/small/'.$images[0]->name)}}" 
                                                          alt=""
                                                          data-zoom-image="{{url($images[0]->path.'/large/'.$images[0]->name)}}"
                                                          >
                                                     <br>          

                                                  @endif  
                                                <div id="gallery_01">
                                                     @foreach($images as $image)

                                                        <a  href="#" class="elevatezoom-gallery active" data-update="" 
                                                            data-image="{{url($image->path.'/small/'.$image->name)}}" 
                                                            data-zoom-image="{{url($image->path.'/large/'.$image->name)}}">
                                                            <img src="{{url($image->path.'/thumb/'.$image->name)}}" alt="" width="100"> 
                                                          </a>
                                                             
                                                      @endforeach 
                                                </div>  
                                                  <br>
                                                  <hr>

                                                  @foreach($features as $feature)
                                                      
                                                      @if($feature->video)  
                                                         <iframe width="400" height="315" src="{{$feature->video}}"  allowfullscreen></iframe>
                                                      @endif    
                                                       
                                                  @endforeach
                                            </div>
                                                @if(Auth::user() && Auth::user()->admin() == 1 )
                                                   <div style="float:right">
                                                       <button class="btn btn-default" aria-label="Left Align" type="submit" data-toggle="modal" data-target="#myModal" >
                                                       <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                                       </button>                                                       
                                                       <button class="btn btn-default" aria-label="Left Align" type="submit" data-toggle="modal" data-target="#myModal2" >
                                                       <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                       </button>

                                                   </div> 
                                                @endif
                                                 <!-- <div class="product-description col-md-10 col-sm-10 col-xs-10"> -->
                                                 <div class="col-md-7 product-description">
                                                    <div class="titulo">
                                                         <p>PRODUCTO EN VENTA</p> 
                                                         <h1>{{$product->item}}</h1>
                                                    </div>
                                                    <div class="col-md-12 box">
                                                        <div class="row">
                                                            <div class="col-md-5">
                                                                <div class="tiempo">
                                                                    <p>TIEMPO DE ENTREGA</p>
                                                                    <h3><span>COMPRA HOY</span> Y RECIBE MAÑANA</h3>
                                                                    <p>(APLICA PARA ECUADOR)</p>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div class="precio">
                                                                    <p>PRECIO</p><br><br>
                                                                    <p style="font-size: 25px">${{$product->price}}</p><br>
                                                                    <h4>DOLARES</h4>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3">
                                                                <div class="Carrito">
                                                                    <div class="btn-toolbar" role="toolbar">
                                                                      <center><button type="button" ng-click="addItem({{$product->code}});" class="btn btn-default btn-lg carrito">
                                                                        <span class="glyphicon glyphicon-shopping-cart"></span>
                                                                      </button></center>
                                                                    </div>
                                                                     <p class="Carritop">AGREGAR AL CARRITO</p>
                                                                     </div>
                                                                 </div>
                                                       </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-12">

                                                            <div class="detalles">
                                                                <h3>DETALLE DEL PRODUCTO</h3>
                                                                @foreach($features as $feature)
                                                                    <p>{{$feature->detail}}<BR>
                                                                    <BR>
                                                                @endforeach
                                                            </div>

                                                        </div>
                                                    </div>
                                                        <div class="row">
                                                            <div class="col-md-12">
                                                                <div class="especificaciones">
                                                                    <h3>ESPECIFICACIONES DEL PRODUCTO</h3>

                                                                    @foreach($features as $feature)

                                                                      @if($feature->features)

                                                                      @foreach(json_decode($feature->features) as $f)
                                                                                 
                                                                        <div class="col-md-6">
                                                                            <div class="panel panel-success">
                                                                                <div class="panel-body2">
                                                                                    <center>{{$f->label}}</center>
                                                                                </div>
                                                                                <div class="panel-footer">
                                                                                <center>                                                                                              @foreach($f->values as $v)
                                                                                            {{$v->value}}                        
                                                                                         @endforeach
                                                                                </center> 
                                                                                </div>
                                                                            </div>
                                                                        </div>                                               
                                                                                

                                                                      @endforeach             
                                                                      @endif
                                                                    @endforeach
                                                              </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                        </div>
                                            </div>
                                       </div>
                                    </div>
                                                <!-- Products Lists Ends -->
                                </div>
                            </div>            <!-- Sidebar Ends -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
<!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
           <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">
                    <span class="glyphicon glyphicon-shopping-cart tamaño"></span> 
                    <b>DETALLAR PRODUCTO</b> 
                </h4>
           </div>
           <div class="modal-body">
                        
                <form 
                action="{{route('publish')}}" 
                method="post" 
                enctype="multipart/form-data" 
                id='formData' 
                style="display:none"
                onsubmit="event.preventDefault()" 
                >
                  {{csrf_field()}}
                  <input type="text" name="data" value="@{{setUp}}">
                  <input type="file" name="image[]"  accept="image/*" multiple id="images">

               </form>
  
               <nav  ng-show='!searchResult'>
                 <div class="nav-wrapper ">
                  <h4 class="brand-logo createnav" >Completa la información del formulario para configurar tu publicacion. </h4>
                              
                 </div>
               </nav>  
       
              <div class="row">
                  
                   <div class="col-md-12">
                    <h3>Detallado</h3>
                    <textarea id="detail" class="form-control" ng-model="pub.detail" rows="12" cols="70"></textarea>


                    <input type="hidden"  id="detail" ng-model="pub.code_id={{$product->code}}" >
                   </div>

                   <div class="col-md-6">
                     <h3>Video</h3>
                     <input type="text" class="form-control" ng-model="pub.video" >
                   </div>
              </div>
              <div class="row">
                <div class="container">
                <h3>Propiedades de tu publicacion</h3>
                    <div class="col-md-12">
                        <div class="card">
                          <div class="card-content">
                              <div class="row">
                                <div class="row" >
                                  <div class="col-md-1 ">
                            
                                      <a class="btn btn-info" ng-click='addFeature()'>
                                        agregar
                                      </a>
                                  </div>

                                  <div class="col-md-5" >
                                    <div>
                                      <input id="titleFeature" class="form-control type="text" ng-model='titleFeature'>
                                      <label for="titleFeature"> Ejemplo: Ancho,Alto,Color</label>
                                    </div>        
                                  </div>

                                </div>


                                <style type="text/css">
                                  table{
                                    padding: 0px;
                                    border-collapse: collapse;
                                    border:solid 1px #c5c5c5;
                                  }
                                  table td{border:solid 1px #c5c5c5;padding: 5px;border-radius: 0px}
                                  .shadow{
                                    -moz-box-shadow:inset 0 0 5px #000000;
                                    -webkit-box-shadow:inset 0 0 5px #000000;
                                    box-shadow:inset 0 0 5px #000000;
                                  }
                                  .input-feature:focus{border:none;}
                                </style>
                            
                                <div ng-repeat='feature in featureList'>
                                    <table class="">
                                      <tbody >
                                      <tr>
                                        <td><b>@{{feature.label}}</b></td>
                                        <td ng-click="deleteFeature($index)" style="padding: 0px;text-align:center;background-color: #414141;color:white;cursor: pointer;">x</td>
                                      </tr>
                                      <tr>
                                        <td>
                                          <input  id="tempFeature@{{$index}}" 
                                                  type="text"
                                                  ng-model="item[$index]"
                                                  ng-blur="addFeatureValues($index,item[$index]); item[$index] = ''"
                                          >
                                        </td>
                                        <td ng-click="addFeatureValues($index,item[$index]); item[$index] = ''" style="padding-left: 5px;text-align:center;background-color: #086e18;color:white;cursor: pointer;">+</td>
                                      </tr>
                                        <tr>
                                          <td style="font-size: 11px;">
                                            <div class="" ng-repeat='child in feature.values'>
                                              @{{child.value}}
                                              <span ng-click="feature.values.splice($index, 1)" style="font-size: 11px">x</span>
                                              
                                            </div>
                                          </td> 
                                          
                                        </tr>
                                      </tbody>
                                      <tr>
                                        
                                      </tr>
                                    </table>
                                    <br>
                                </div>  
                              </div>

                          </div>
                        </div>
                    </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <h3>Imagenes</h3>
                  <p>Selecciona las imagenes de tu publicacion.</p>

                  
                  <div >
                   <buttn class="btn btn-info" ng-click='fackefilebutton()'>Subir imagenes</button>
                  </div>

                  <div class="row">
                      <div class="col s12">
                         <div id="list"></div>
                        
                      </div> 
                  </div>
                  <a class="btn btn-success" ng-click="publish()" style="float:right;margin-bottom: 1em;margin-top: 2em">Publicar!</a>

                </div>
              </div>
            </div>
        </div>
      </div>
    </div>
{{-- modal2     --}}
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
           <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title " id="myModalLabel" >
                    <span class="glyphicon glyphicon-alert tamaño "></span> 
                    <b>Eliminar Contenido <span class="label label-warning">Warning</span></b> <br><br>
                     <h4> Esta seguro que quiere eliminar la descripcion y las especificaciones de este producto?</h4>
                </h4>
           </div>

           <div class="modal-footer">
               <form action="{{ route('publishDelete',$product->code )}}" method="post">
                                  <input type="hidden" name="_method" value="DELETE">
                                  {{ csrf_field() }}
                                  <button type="submit" class="btn btn-success" style="float:right">Si</button>
              </form>
              <a href=""  data-dismiss="modal" class="btn btn-danger" style="float:right;margin-right:1em;" >No</a>
             
           </div>
        </div>
      </div>
    </div>


    <script type="text/javascript">
  
         function archivo(evt){
                document.getElementById("list").innerHTML = "";
                var files = evt.target.files; // FileList object
                 
                  //Obtenemos la imagen del campo "file". 
                for (var i = 0; f = files[i]; i++){        
                     //Solo admitimos imágenes.
                     if (!f.type.match('image.*')){
                          continue;
                     }
                 
                     var reader = new FileReader();
                     
                     reader.onload = (function(theFile) {
                         return function(e) {
                         // Creamos la imagen.
                      document.getElementById("list").innerHTML += ['<div class="col-md-3"><img class="img-circle img-responsive" src="', e.target.result,'"/></div>'].join('');
                         };
                     })(f);
           
                     reader.readAsDataURL(f);
                 }
          }
                       
      document.getElementById('images').addEventListener('change', archivo, false);
</script>


<script type="text/javascript">

  var zoomConfig = {
    cursor: 'crosshair',
    zoomType: "window",
    zoomWindowWidth:600,
    zoomWindowHeight:400,
    borderSize:1
  }; 
  
  var image = $('#gallery_01 a');
  var zoomImage = $('img#zoom_03');

  zoomImage.elevateZoom(zoomConfig);//initialise zoom

  image.on('click', function(){
      // Remove old instance od EZ
      $('.zoomContainer').remove();
      zoomImage.removeData('elevateZoom');
      // Update source for images
      zoomImage.attr('src', $(this).data('image'));
      zoomImage.data('zoom-image', $(this).data('zoom-image'));
      // Reinitialize EZ
      zoomImage.elevateZoom(zoomConfig);
  });

</script>
@endsection
